// DOM - document object model

const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
// querySelector function takes a string input that is formatted like CSS selector when applying styles. This allows to get a specific element.

// alternative
// document.getElementById('txt-first-name')

// document.getElementsByClassName()
// document.getElementsByTagName()

/*
	Mini activity
		*Target the element full name
		*/
const spanFullName = document.querySelector('#span-whole-name')


txtFirstName.addEventListener('keyup' , doSomething);
txtLastName.addEventListener('keyup' , doSomething);

function doSomething(e) {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}

// txtFirstName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
// })

// txtLastName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
// })












